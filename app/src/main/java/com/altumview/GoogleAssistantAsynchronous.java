package com.altumview;

import android.os.AsyncTask;
import android.util.Log;

import com.mautini.assistant.demo.androidaudio.AudioPlayer;
import com.mautini.assistant.demo.androidaudio.AudioRecorder;
import com.mautini.assistant.demo.api.AssistantClient;
import com.mautini.assistant.demo.authentication.AuthenticationHelper;
import com.mautini.assistant.demo.config.AssistantConf;
import com.mautini.assistant.demo.config.AudioConf;
import com.mautini.assistant.demo.config.AuthenticationConf;
import com.mautini.assistant.demo.config.DeviceRegisterConf;
import com.mautini.assistant.demo.config.IoConf;
import com.mautini.assistant.demo.device.DeviceRegister;
import com.mautini.assistant.demo.exception.AudioException;
import com.mautini.assistant.demo.exception.AuthenticationException;
import com.mautini.assistant.demo.exception.ConverseException;
import com.mautini.assistant.demo.exception.DeviceRegisterException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigBeanFactory;
import com.typesafe.config.ConfigFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Scanner;

import io.grpc.helloworldexample.Communicator;

public class GoogleAssistantAsynchronous extends AsyncTask<Communicator, Void, Boolean>
    {

        Communicator communicator;
        String thisContextCode;
        @Override
        protected Boolean doInBackground(Communicator... communicators) {

            this.communicator = communicators[0];
            this.thisContextCode = communicator.thisContextCode;

            return runGoogleAssistantHelper();
        }

        public Boolean runGoogleAssistantHelper(){
            Log.v("STAGE","Running helper...");
            Config root = ConfigFactory.load();
            AuthenticationConf authenticationConf = ConfigBeanFactory.create(root.getConfig("authentication"), AuthenticationConf.class);

            DeviceRegisterConf deviceRegisterConf = ConfigBeanFactory.create(root.getConfig("deviceRegister"), DeviceRegisterConf.class);
            AssistantConf assistantConf = ConfigBeanFactory.create(root.getConfig("assistant"), AssistantConf.class);
            AudioConf audioConf = ConfigBeanFactory.create(root.getConfig("audio"), AudioConf.class);
            IoConf ioConf = ConfigBeanFactory.create(root.getConfig("io"), IoConf.class);
            try {


                //TODO: DELETE THIS
                //SynchronizeNetworking.enqueueAndBlock(oAuthClient, code, authenticationConf)



                // Authentication
                AuthenticationHelper authenticationHelper = new AuthenticationHelper(authenticationConf,thisContextCode);
                authenticationHelper
                        .authenticate()
                        .orElseThrow(() -> new AuthenticationException("Error during authentication"));

                // Check if we need to refresh the access token to request the api
                if (authenticationHelper.expired()) {
                    authenticationHelper
                            .refreshAccessToken()
                            .orElseThrow(() -> new AuthenticationException("Error refreshing access token"));
                }

                // Register Device model and device
                DeviceRegister deviceRegister = new DeviceRegister(deviceRegisterConf, authenticationHelper.getOAuthCredentials().getAccessToken());
                deviceRegister.register();
                // Build the client (stub)
                AssistantClient assistantClient = new AssistantClient(authenticationHelper.getOAuthCredentials(), assistantConf,
                        deviceRegister.getDeviceModel(), deviceRegister.getDevice(), ioConf);

                // Build the objects to record and play the conversation

                //TODO: AUDIO REPLACEMENT
                AudioRecorder audioRecorder = new AudioRecorder(audioConf,communicator);
                AudioPlayer audioPlayer = new AudioPlayer(audioConf);

                // Initiating Scanner to take user input
                Scanner scanner = new Scanner(System.in);

                // Main loop
                while (true) {

                    Log.i("GOOGLE_ASSISTANT_LOG","New iteration in loop");

                    // Get input (text or voice)
                    try {
                        // Check if we need to refresh the access token to request the api
                        if (authenticationHelper.expired()) {
                            authenticationHelper
                                    .refreshAccessToken()
                                    .orElseThrow(() -> new AuthenticationException("Error refreshing access token"));

                            // Update the token for the assistant client
                            assistantClient.updateCredentials(authenticationHelper.getOAuthCredentials());
                        }


                        byte[] request = getInput(ioConf, scanner, audioRecorder);

                        // plays back sound input
                        //Log.w("AUDIOTEST","Playing sound");
                        //audioPlayer.play(request);
                        //Log.w("AUDIOTEST","Got past playoing song");



                        // requesting assistant with text query
                        assistantClient.requestAssistant(request);

                        try {
                            Log.w("ANDROID_WORDS", assistantClient.getTextResponse());
                        } catch (Throwable t){
                            Log.w("ANDROID_WORDS", "Failed to get a response");
                            Log.w("ERROR", t.toString());
                        }
                        if (Boolean.TRUE.equals(ioConf.getOutputAudio())) {

                            byte[] audioResponse = assistantClient.getAudioResponse();
                            if (audioResponse.length > 0) {
                                audioPlayer.play(audioResponse);
                            } else {
                                Log.w("ANDROID_WORDS", "No response from the API");
                            }
                        } else {
                            Log.w("ANDROID_WORDS", "No audio response");

                        }

                    } catch (AudioException e) {
                        Log.e("AUDIOEXCEPTION", "Caught audio exception");

                    } catch (ConverseException e) {
                        Log.e("ConverseException", "Caught ConverseException");

                    } catch (AuthenticationException e) {
                        Log.e("AuthenticationException", "Caught AuthenticationException");

                    } catch (Throwable e) {
                        Log.e("Throwable", "Caught Throwable");
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        e.printStackTrace(pw);
                        String sStackTrace = sw.toString(); // stack trace as a string
                        Log.e("ERROR:",sStackTrace);


                    }

                }
            } catch (AuthenticationException e) {
                Log.w("AuthenticationException","AuthenticationException found");

            } catch (DeviceRegisterException e){
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                String sStackTrace = sw.toString(); // stack trace as a string

                Log.w("DeviceRegisterException",sStackTrace);

            }
            return true;
        }


        private byte[] getInput(IoConf ioConf, Scanner scanner, AudioRecorder audioRecorder) throws AudioException {


            switch (ioConf.getInputMode()) {
                case IoConf.TEXT:
                    Log.i("LOGGER","Tap your request and press enter...");
                    // Taking user input in
                    String query = scanner.nextLine();
                    // Converting user into byte array to keep consistency of requestAssistant params
                    return query.getBytes();
                case IoConf.AUDIO:
                    // Record
                    return audioRecorder.getRecord();
                default:
                        Log.e("LOGGER","Unknown input mode {}"+ ioConf.getInputMode());
                    return new byte[]{};
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            Log.v("SUCCESSFUL NETWORKING:","Finished googleassistantasynchronous");

        }

    }