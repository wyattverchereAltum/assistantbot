package com.altumview;

import com.mautini.assistant.demo.authentication.OAuthCredentials;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

    public class OauthCredentialsFuture implements Future<OAuthCredentials> {

        private volatile OAuthCredentials result = null;
        private volatile boolean cancelled = false;
        private final CountDownLatch countDownLatch;

        public OauthCredentialsFuture() {
            countDownLatch = new CountDownLatch(1);
        }

        @Override
        public boolean cancel(final boolean mayInterruptIfRunning) {
            if (isDone()) {
                return false;
            } else {
                countDownLatch.countDown();
                cancelled = true;
                return !isDone();
            }
        }

        @Override
        public OAuthCredentials get() throws InterruptedException, ExecutionException {
            countDownLatch.await();
            return result;
        }

        @Override
        public OAuthCredentials get(final long timeout, final TimeUnit unit)
                throws InterruptedException, ExecutionException, TimeoutException {
            countDownLatch.await(timeout, unit);
            return result;
        }

        @Override
        public boolean isCancelled() {
            return cancelled;
        }

        @Override
        public boolean isDone() {
            return countDownLatch.getCount() == 0;
        }

        public void onBazResult(final OAuthCredentials result) {
            this.result = result;
            countDownLatch.countDown();
        }

    }
