package com.altumview;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class RequestTask extends AsyncTask<String, String, String> {

    @Override
    protected String doInBackground(String... uri) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = null;
        try {
            Log.v("STAGE","Send");
            response = httpclient.execute(new HttpGet(uri[0]));
            Log.v("STAGE","Send1");

            StatusLine statusLine = response.getStatusLine();
            Log.v("STAGE","Send2");
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                Log.v("STAGE","ok");

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                Log.v("STAGE","ok1");

                response.getEntity().writeTo(out);
                Log.v("STAGE","ok2");

                responseString = out.toString();
                Log.v("STAGE","ok3");

                out.close();
            } else{
                //Closes the connection.
                Log.v("STAGE","nope");

                response.getEntity().getContent().close();
                Log.v("STAGE","nope1");

                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string
            Log.e("nw ClientProtocolExcep:",sStackTrace);
        } catch (IOException e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string
            Log.e("network IOException:",sStackTrace);
        } catch (Exception e){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string
            Log.e("network other:",sStackTrace);

        }
        Log.v("STAGE","end this");
        Log.v("REs",responseString);

        return responseString;
    }

    @Override
    protected void onPostExecute(String result) {
        Log.v("STAGE","onpostexecute");

        super.onPostExecute(result);
        Log.v("SUCCESSFUL NETWORKING:",result);
    }
}