package com.altumview;

import android.util.Log;

import com.mautini.assistant.demo.authentication.OAuthClient;
import com.mautini.assistant.demo.authentication.OAuthCredentials;
import com.mautini.assistant.demo.config.AuthenticationConf;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;




public class SynchronizeNetworking {
    public static Future<OAuthCredentials> doSomething(OAuthClient oAuthClient, String code, AuthenticationConf authenticationConf) {
        OauthCredentialsFuture future = new OauthCredentialsFuture();

        //doSomethingAsync(fooArg, barArg, future);

        Callback<OAuthCredentials> cb = new Callback<OAuthCredentials>() {
            @Override
            public void onResponse(Call<OAuthCredentials> call, Response<OAuthCredentials> response) {
                Log.v("STAGE","Responded");

                future.onBazResult(response.body());
            }
            @Override
            public void onFailure(Call<OAuthCredentials> call, Throwable t) {
                Log.e("NETWORK ERROR","Could not successfully connect");
            }
        };

        Log.v("STAGE","About to enqueue function");

        oAuthClient.getAccessToken(
                code,
                authenticationConf.getClientId(),
                authenticationConf.getClientSecret(),
                authenticationConf.getCodeRedirectUri(),
                "authorization_code")
                .enqueue(cb);
        Log.v("STAGE","Ran enqueue function");



        return future;
    }

    public static OAuthCredentials enqueueAndBlock(OAuthClient oAuthClient, String code, AuthenticationConf authenticationConf) {
        OAuthCredentials o;
        try {
            Log.v("STAGE","Enqueued");

            o = doSomething(oAuthClient, code, authenticationConf).get();
        } catch (InterruptedException | ExecutionException e){
            Log.e("EXCEPTION","Error found executing, or it was interrupted");
            o = new OAuthCredentials();
        }
        return o;
    }
}

