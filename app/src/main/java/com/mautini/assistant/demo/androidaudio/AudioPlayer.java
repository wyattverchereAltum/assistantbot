package com.mautini.assistant.demo.androidaudio;

import android.annotation.TargetApi;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTrack;
import android.os.Build;

import com.mautini.assistant.demo.androidaudio.AudioUtil;
import com.mautini.assistant.demo.config.AudioConf;
import com.mautini.assistant.demo.exception.AudioException;


public class AudioPlayer {

    private AudioConf audioConf;

    public AudioPlayer(AudioConf audioConf) {
        this.audioConf = audioConf;
    }

    @TargetApi(Build.VERSION_CODES.CUR_DEVELOPMENT)
    public void play(byte[] sound) throws AudioException {
        try {

            AudioTrack audioTrack = new AudioTrack.Builder().setAudioAttributes(new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build())
                    .setAudioFormat(AudioUtil.getAudioFormat(audioConf))
                    .setBufferSizeInBytes(sound.length)
                    .setTransferMode(AudioTrack.MODE_STREAM)
                    .build();
            audioTrack.play();
            audioTrack.write(sound,0,sound.length);
            audioTrack.stop();
        } catch (Exception e) {
            throw new AudioException("Unable to play the response", e);
        }
    }
}
