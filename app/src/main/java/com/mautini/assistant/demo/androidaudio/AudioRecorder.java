package com.mautini.assistant.demo.androidaudio;

import android.annotation.TargetApi;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Build;
import android.util.Log;

import com.googlecode.openbeans.BeanInfo;
import com.mautini.assistant.demo.config.AudioConf;
import com.mautini.assistant.demo.exception.AudioException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import io.grpc.helloworldexample.Communicator;

public class AudioRecorder {

    private static final Logger LOGGER = LoggerFactory.getLogger(AudioRecorder.class);

    private boolean stopped = false;
    private AudioConf audioConf;
    private AudioRecord recorder;

    private boolean endLoop = false;

    Communicator communicator;


    public AudioRecorder(AudioConf audioConf, Communicator communicator) {
        this.audioConf = audioConf;
        this.communicator = communicator;
    }

    public byte[] getRecord() throws AudioException {

        try {
            // Reset the flag
            stopped = false;

            // Start a new thread to wait during listening
            Thread stopper = new Thread(() -> {
                    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                    //br.readLine();
                Log.i("GOOGLE_ASSISTANT_LOG","Creating new waiting thread...");

                while(!communicator.getShouldStartRecording()){}

                Log.i("GOOGLE_ASSISTANT_LOG","Manager thread now recording...");

                while(!communicator.getShouldFinishCapture()){}
                    communicator.finishCapture();

                    stopped = true;
                    Log.i("GOOGLE_ASSISTANT_LOG","End of the capture");

                stop();
            });

            // Start the thread that can stop the record
            stopper.start();

            return record();
        } catch (Exception e) {
            throw new AudioException("Unable to record your voice", e);
        }
    }


    @TargetApi(Build.VERSION_CODES.CUR_DEVELOPMENT)
    private byte[] record() {

        recorder = new AudioRecord.Builder()
                .setAudioSource(MediaRecorder.AudioSource.MIC)
                .setAudioFormat(new AudioFormat.Builder()
                    .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                        .setChannelMask(AudioFormat.CHANNEL_IN_MONO)
                    .setSampleRate(16000)
                    .build()).setBufferSizeInBytes(8192)
                .build();

        Log.i("STAGE","Waiting to start recording again...");

        while(!communicator.getShouldStartRecording()){}


        recorder.startRecording();

        Log.i("STAGE","Listening, tap enter to stop ...");

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();


        short sData[] = new short[8192];
        while(!stopped){
            int numread = recorder.read(sData,0, 8192);
            if(numread > 0){
                for (int i = 0; i < numread; ++i) {
                    sData[i] = (short)Math.min((int)(sData[i] * 40), (int)Short.MAX_VALUE);
                }
            }
            byte bData[] = short2byte(sData);
            byteArrayOutputStream.write(bData,0,8192*2);
        }
        return byteArrayOutputStream.toByteArray();

    }



    //convert short to byte
    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;

    }

    /**
     * Stop the capture
     */
    private void stop() {
        if(recorder != null){
            recorder.stop();
            recorder.release();

        }
    }
}
