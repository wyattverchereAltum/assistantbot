package com.mautini.assistant.demo.androidaudio;

import android.annotation.TargetApi;
import android.media.AudioFormat;
import android.os.Build;

import com.mautini.assistant.demo.config.AudioConf;


public class AudioUtil {
    /**
     * Defines the audio format for requests and responses
     */

    @TargetApi(Build.VERSION_CODES.CUR_DEVELOPMENT)
    public static AudioFormat getAudioFormat(AudioConf audioConf) {
        return new AudioFormat.Builder().setEncoding(AudioFormat.ENCODING_DEFAULT).setSampleRate(audioConf.getSampleRate()).setChannelIndexMask(audioConf.getChannels()).build();

        /*return new AudioFormat(
                audioConf.getSampleRate(),
                audioConf.getSampleSizeInBits(),
                audioConf.getChannels(),
                audioConf.getSigned(),
                audioConf.getBigEndian());*/
    }
}
