package com.mautini.assistant.demo.authentication;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.altumview.RequestTask;
import com.altumview.SynchronizeNetworking;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.mautini.assistant.demo.config.AuthenticationConf;
import com.mautini.assistant.demo.exception.AuthenticationException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.awt.*;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.SynchronousQueue;



public class AuthenticationHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationHelper.class);

    // The current credentials for the app
    private OAuthCredentials oAuthCredentials;

    // The client to perform HTTP request for oAuth2 authentication
    private OAuthClient oAuthClient;

    // The Gson object to store the credentials in a file
    private Gson gson;

    // The configuration for the authentication module (see reference.conf in resources)
    private AuthenticationConf authenticationConf;

    private String code;

    public AuthenticationHelper(AuthenticationConf authenticationConf, String ct) {
        Log.v("STAGE","start of authentication helper constructor");

        this.code = ct;

        this.authenticationConf = authenticationConf;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(authenticationConf.getGoogleOAuthEndpoint())
                .addConverterFactory(GsonConverterFactory.create())
                .build();



        /*
        *
        *
        * TODO: DELETE ME
        *
        *
        *
        * */
            //AsyncTask<String, String, String> rq = new RequestTask().execute("http://httpbin.org/get");
            //Log.v("STAGE", "Called the test function");
        /*
         *
         *
         * TODO: DELETE ME
         *
         *
         *
         * */

        oAuthClient = retrofit.create(OAuthClient.class);
        gson = new Gson();
        Log.v("STAGE","End of authentication helper constructor");
    }

    public OAuthCredentials getOAuthCredentials() {
        return oAuthCredentials;
    }

    public Optional<OAuthCredentials> authenticate() throws AuthenticationException {
        Log.v("STAGE","Start of authenticate");

        try {
            Log.v("STAGE","Initiating file search");

            File file = new File(authenticationConf.getCredentialsFilePath());
            Log.v("STAGE","Looking for file");

            if (file.exists()) {
                Log.i("GOOGLE_ASSISTANT_LOG","Loading oAuth credentials from file");
                // If we have previous credentials in a file, use them
                oAuthCredentials = gson.fromJson(new JsonReader(new FileReader(authenticationConf.getCredentialsFilePath())), OAuthCredentials.class);
                Log.i("GOOGLE_ASSISTANT_LOG","Access Token: " + oAuthCredentials.getAccessToken());
            } else {
                Log.v("STAGE","No file");

                // Create new credentials
                Optional<OAuthCredentials> optCredentials = requestAccessToken();
                Log.v("STAGE","Requesting an access token");

                if (optCredentials.isPresent()) {
                    oAuthCredentials = optCredentials.get();
                    Log.i("GOOGLE_ASSISTANT_LOG","Access Token: " + oAuthCredentials.getAccessToken());
                    saveCredentials();
                }
            }
            return Optional.of(oAuthCredentials);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string
            Log.e("STACK TRACE:",sStackTrace);
            throw new AuthenticationException("Error during authentication", e);
        }
    }

    /**
     * Check if the token is expired
     *
     * @return true if the access token need to be refreshed false otherwise
     */
    public boolean expired() {
        // Add a delay to be sure to not make a request with an expired token

        Log.e("CREDENTIALS","Assuming valid token");
        return false;

        //return oAuthCredentials.getExpirationTime() - System.currentTimeMillis() < authenticationConf.getMaxDelayBeforeRefresh();
    }

    /**
     * Refresh the access token for oAuth authentication
     *
     * @return the new (refreshed) credentials
     */
    public Optional<OAuthCredentials> refreshAccessToken() throws AuthenticationException {
        Log.i("GOOGLE_ASSISTANT_LOG","Refreshing access token");
        try {
            Response<OAuthCredentials> response = oAuthClient.refreshAccessToken(
                    oAuthCredentials.getRefreshToken(),
                    authenticationConf.getClientId(),
                    authenticationConf.getClientSecret(),
                    "refresh_token")
                    .execute();

            if (response.isSuccessful()) {
                Log.i("GOOGLE_ASSISTANT_LOG","New Access Token: " + response.body().getAccessToken());
                oAuthCredentials.setAccessToken(response.body().getAccessToken());
                oAuthCredentials.setExpiresIn(response.body().getExpiresIn());
                oAuthCredentials.setTokenType(response.body().getTokenType());
                saveCredentials();
                return Optional.of(oAuthCredentials);
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            throw new AuthenticationException("Error during authentication", e);
        }
    }

    /**
     * Request an access token by asking the user to authorize the application
     *
     * @return credentials if the request succeeds
     * @throws URISyntaxException if the request fails
     * @throws IOException        if the request fails
     */

    private Optional<OAuthCredentials> requestAccessToken() throws URISyntaxException, IOException {
        String url = "https://accounts.google.com/o/oauth2/v2/auth?" +
                "scope=" + authenticationConf.getScope() + "&" +
                "response_type=code&" +
                "redirect_uri=" + authenticationConf.getCodeRedirectUri() + "&" +
                "client_id=" + authenticationConf.getClientId();

        Log.v("STAGE","B4 execute");

        // TODO: IMPLEMENT https://stackoverflow.com/questions/2180419/wrapping-an-asynchronous-computation-into-a-synchronous-blocking-computation SOMETHING LIKE THIS
        // TODO: SOMETHING LIKE THAT
        // TODO: MNEEDS TO WORK SYNCHRONOUSL


        Log.v("STAGE","After execute");

        oAuthCredentials = SynchronizeNetworking.enqueueAndBlock(oAuthClient, code, authenticationConf);


        /*if (response.isSuccessful()) {

            oAuthCredentials = response.body();

            return Optional.of(oAuthCredentials);
        } else {
            Log.v("STAGE","Failed response");
            return Optional.empty();

        }*/
        Log.v("STAGE","Stuck");
        return Optional.of(oAuthCredentials);

    }

    /**
     * Save the credentials in a file
     *
     * @throws IOException if the file cannot be created
     */
    private void saveCredentials() throws IOException {

        Log.e("FAILURE","Did not save credentials");
        oAuthCredentials.setExpirationTime(System.currentTimeMillis() + oAuthCredentials.getExpiresIn() * 1000);

        /*try (FileWriter writer = new FileWriter(authenticationConf.getCredentialsFilePath())) {
            // Set the expiration Date
            oAuthCredentials.setExpirationTime(System.currentTimeMillis() + oAuthCredentials.getExpiresIn() * 1000);
            gson.toJson(oAuthCredentials, writer);
        }*/
    }
}
