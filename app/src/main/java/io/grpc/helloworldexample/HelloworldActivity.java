/*
 * Copyright 2015 The gRPC Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.grpc.helloworldexample;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.assistant.embedded.v1alpha2.EmbeddedAssistantGrpc;
import com.mautini.assistant.demo.androidaudio.AudioPlayer;
import com.mautini.assistant.demo.androidaudio.AudioRecorder;
import com.mautini.assistant.demo.api.AssistantClient;
import com.mautini.assistant.demo.authentication.AuthenticationHelper;
import com.mautini.assistant.demo.config.AssistantConf;
import com.mautini.assistant.demo.config.AudioConf;
import com.mautini.assistant.demo.config.AuthenticationConf;
import com.mautini.assistant.demo.config.DeviceRegisterConf;
import com.mautini.assistant.demo.config.IoConf;
import com.mautini.assistant.demo.device.DeviceRegister;
import com.mautini.assistant.demo.exception.AudioException;
import com.mautini.assistant.demo.exception.AuthenticationException;
import com.mautini.assistant.demo.exception.ConverseException;
import com.mautini.assistant.demo.exception.DeviceRegisterException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigBeanFactory;
import com.typesafe.config.ConfigFactory;

import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.examples.helloworld.GreeterGrpc;
import io.grpc.examples.helloworld.HelloReply;
import io.grpc.examples.helloworld.HelloRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import io.grpc.examples.helloworld.*;

public class HelloworldActivity extends AppCompatActivity {
  private Button sendButton;
  private EditText authEdit;
  private EditText portEdit;
  private EditText messageEdit;
  private TextView resultText;

  private EmbeddedAssistantGrpc.EmbeddedAssistantStub embeddedAssistantStub;

  private String thisContextCode;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    doPermissions();


    setContentView(R.layout.activity_helloworld);

    sendButton = (Button) findViewById(R.id.send_button);
    authEdit = (EditText) findViewById(R.id.host_edit_text);
    resultText = (TextView) findViewById(R.id.grpc_response_text);
    resultText.setMovementMethod(new ScrollingMovementMethod());

    Config root = ConfigFactory.load();
    AuthenticationConf authenticationConf = ConfigBeanFactory.create(root.getConfig("authentication"), AuthenticationConf.class);

    getAuthCode(authenticationConf);
  }


  public void getAuthCode(AuthenticationConf authenticationConf){
    String url = "https://accounts.google.com/o/oauth2/v2/auth?" +
            "scope=" + authenticationConf.getScope() + "&" +
            "response_type=code&" +
            "redirect_uri=" + authenticationConf.getCodeRedirectUri() + "&" +
            "client_id=" + authenticationConf.getClientId();

    // Open a browser to authenticate using oAuth2
    //Desktop.getDesktop().browse(new URI(url));

    /*
     *
     *
     * New android version
     *
     *
     *
     * */
    //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    //context.startActivity(browserIntent);
    //LOGGER.info("Allow the application in your browser and copy the authorization code in the console");
    //BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    //String code = br.readLine();

    //TODO: Read actual token from website rather than manual input

    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    startActivity(browserIntent);

  }



  public void sendMessage(View view) {

    Intent i=new Intent(getApplicationContext(),Communicator.class);
    i.putExtra("AUTHCODE", authEdit.getText().toString());
    startActivity(i);

  }


  private int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 5;
  private int MY_PERMISSIONS_REQUEST_MODIFY_AUDIO_SETTINGS = 6;
  private int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 7;
  private  int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 8;
  private void doPermissions(){


    // Here, thisActivity is the current activity
    if (ContextCompat.checkSelfPermission(this,
            Manifest.permission.RECORD_AUDIO)
            != PackageManager.PERMISSION_GRANTED) {

      // Permission is not granted
      // Should we show an explanation?
      if (ActivityCompat.shouldShowRequestPermissionRationale(this,
              Manifest.permission.RECORD_AUDIO)) {
        // Show an explanation to the user *asynchronously* -- don't block
        // this thread waiting for the user's response! After the user
        // sees the explanation, try again to request the permission.
      } else {
        // No explanation needed; request the permission
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.RECORD_AUDIO},
                MY_PERMISSIONS_REQUEST_RECORD_AUDIO);

        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
        // app-defined int constant. The callback method gets the
        // result of the request.
      }
    } else {
      // Here, thisActivity is the current activity
      if (ContextCompat.checkSelfPermission(this,
              Manifest.permission.MODIFY_AUDIO_SETTINGS)
              != PackageManager.PERMISSION_GRANTED) {

        // Permission is not granted
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.MODIFY_AUDIO_SETTINGS)) {
          // Show an explanation to the user *asynchronously* -- don't block
          // this thread waiting for the user's response! After the user
          // sees the explanation, try again to request the permission.
        } else {
          // No explanation needed; request the permission
          ActivityCompat.requestPermissions(this,
                  new String[]{Manifest.permission.MODIFY_AUDIO_SETTINGS},
                  MY_PERMISSIONS_REQUEST_MODIFY_AUDIO_SETTINGS);

          // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
          // app-defined int constant. The callback method gets the
          // result of the request.
        }
      } else {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

          // Permission is not granted
          // Should we show an explanation?
          if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                  Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
          } else {
            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
          }
        } else {
          // Here, thisActivity is the current activity
          if (ContextCompat.checkSelfPermission(this,
                  Manifest.permission.READ_EXTERNAL_STORAGE)
                  != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
              // Show an explanation to the user *asynchronously* -- don't block
              // this thread waiting for the user's response! After the user
              // sees the explanation, try again to request the permission.
            } else {
              // No explanation needed; request the permission
              ActivityCompat.requestPermissions(this,
                      new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                      MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

              // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
              // app-defined int constant. The callback method gets the
              // result of the request.
            }
          } else {
            Log.v("STAGE","ALL PERMISSIONS GRANTED");
          }
        }
      }
    }


  }

}
